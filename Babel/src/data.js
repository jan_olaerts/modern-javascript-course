const users = [
    {name: 'mario', premium: true},
    {name: 'luigi', premium: false},
    {name: 'ryu', premium: true},
    {name: 'toad', premium: true},
    {name: 'peach', premium: false}
];

const getPremUsers = (users) => {
   return users.filter(user => user.premium);
}

export {getPremUsers, users as default};