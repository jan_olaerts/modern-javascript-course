const list = document.querySelector('ul');
const form = document.querySelector('form');
const button = document.querySelector('button');

const unsub = db.collection('recipes').onSnapshot(snapshot => {
    snapshot.docChanges().forEach(change => {
        const doc = change.doc;
        if(change.type === 'added'){
            addRecipe(doc.data(), doc.id);
        }
        if(change.type === 'removed'){
            deleteRecipe(doc.id);
        }
    })
})

button.addEventListener('click', () => {
    unsub();
})

const addRecipe = (recipe, id) => {
    let time = recipe.created_at.toDate();
    let html = `
    <li data-id="${id}">
        <div>${recipe.title}</div>
        <div>${time}</div>
        <button class="btn btn-danger btn-sm my-2">Delete</button>
    </li>`;

    list.innerHTML += html;
}

form.addEventListener('submit', e => {
    e.preventDefault();

    const now = new Date();
    
    const recipe = {
        created_at: firebase.firestore.Timestamp.fromDate(now),
        title: form.recipe.value
    };
    db.collection('recipes').add(recipe).then(() => {
        addRecipe(recipe);
    }).catch(error => console.log(error));
})

list.addEventListener('click', e => {
    if(e.target.tagName === 'BUTTON'){
        const id = e.target.parentElement.getAttribute('data-id');
        db.collection('recipes').doc(id).delete();
    }
})

const deleteRecipe = (id) => {
    const recipes = document.querySelectorAll('li');
    recipes.forEach(recipe => {
        if(recipe.getAttribute('data-id') === id){
            recipe.remove();
        }
    })
}
