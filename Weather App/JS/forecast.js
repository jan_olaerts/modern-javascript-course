const key = 'nMB1dKlzN1GFIEbZOYRgHGu48zBEdMmb';

//get weather information
const getWeather = async (id) => {
    const base = 'http://dataservice.accuweather.com/currentconditions/v1/';
    const query = `${id}?apikey=${key}`;

    const response = await fetch(base + query);
    const data = await response.json();

    return data[0];
}

//get city information
const getLocationKey = async (location) => {

    const base = 'http://dataservice.accuweather.com/locations/v1/cities/search';
    const query = `?apikey=${key}&q=${location}`;
    
    const response = await fetch(base + query);
    const data = await response.json();

   return data[0];
}