const locationForm = document.querySelector('.change-location');
const card = document.querySelector('.card');
const details = document.querySelector('.details');
const time = document.querySelector('img.time');
const icon = document.querySelector('.icon');

const locationUpdate = async (location) => {

    const locationDetails = await getLocationKey(location);
    const weather = await getWeather(locationDetails.Key);

    return {locationDetails, weather}
}

locationForm.addEventListener('submit', e => {
    e.preventDefault();

    const location = locationForm.city.value.trim();
    locationForm.reset();

    localStorage.setItem('location', location);

    locationUpdate(location)
        .then(data => updateUI(data));
})

const updateUI = (data) => {

    const {locationDetails, weather} = data;

    details.innerHTML = `<h5 class="my-3">${locationDetails.LocalizedName}</h5>
    <div class="my-3">${weather.WeatherText}</div>
    <div class="display-4 my-4">
        <span>${weather.Temperature.Metric.Value}</span>
        <span>°C</span>
    </div>`;

    if(card.classList.contains('d-none')){
        card.classList.remove('d-none');
    }

    let timeSrc = null;
    if(weather.IsDayTime){
        timeSrc = 'img/day.svg';
    }

    else{
        timeSrc = 'img/night.svg';
    }

    time.src = timeSrc;

    icon.innerHTML = `<img src='img/icons/${weather.WeatherIcon}.svg'>`;
}

if(localStorage.getItem('location')){
    locationUpdate(localStorage.getItem('location'))
        .then(data => updateUI(data))
        .catch(error => (error));
}