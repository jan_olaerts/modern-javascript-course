// Adding new todos
const addForm = document.querySelector('.add');
const list = document.querySelector('ul');

const generateTemplate = (todo => {

    const html =    `<li class="list-group-item d-flex justify-content-between align-items-center">
                    <span>${todo}</span>
                    <i class="far fa-trash-alt delete"></i>
                    </li>`;

    list.innerHTML += html;

})

addForm.addEventListener('submit', e => {
    e.preventDefault();

    const todo = addForm.add.value.trim().toLowerCase();

    if(todo.length){
        generateTemplate(todo);
    }

    addForm.reset();
})

// Deleting todos
list.addEventListener('click', e => {
    if(e.target.tagName === 'I'){
        e.target.parentElement.remove();
    }
})

//Searching and filtering todos
const search = document.querySelector('.search input');



search.addEventListener('keyup', ()  => {
    const term = search.value.trim().toLowerCase();

    filterTodos(term);
})

const filterTodos = term => {
    Array.from(list.children)
        .filter(item => !item.textContent.toLowerCase().includes(term))
        .forEach(item => item.classList.add('hide'));

        Array.from(list.children)
        .filter(item => item.textContent.toLowerCase().includes(term))
        .forEach(item => item.classList.remove('hide'));
}