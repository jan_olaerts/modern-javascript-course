const correctAnswers = ['B', 'B', 'B', 'B'];
const form = document.querySelector('.quiz-form');
const result = document.querySelector('.result');

// Check answers
form.addEventListener('submit', e => {
    e.preventDefault();

    const userAnswers= [form.q1.value, form.q2.value, form.q3.value, form.q4.value];
    let score = 0; 

    userAnswers.forEach((userAnswer, index) => {
        if(userAnswer === correctAnswers[index]){
            score += 25;
        }
    })

// Show score
result.classList.remove('d-none');
scrollTo(0,0);

// Animating the score
let output = 0;

const countUp = setInterval(() => {
    result.querySelector('span').textContent = `${output}%`;
    if(output === score){
        clearInterval(countUp);
    }

    else{
        output++;
    }
},10)

})



